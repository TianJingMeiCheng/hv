## 还没有做好，请不要复制，好le通知!!!
### 起点

集微信支付、支付宝支付融合制作一款线下中小商户营销管理系统，尽可能的根据线下实际场景应用帮助商户拓展对接线上营销管理的全方位系统

### 构思

- 集成微信、支付宝支付系统
- 小型且可满足的入库销存、OA模块
- PC端管理系统
- 符合零售商户的线上商城购买商城系统，主要为微信平台
- 线上消息、广告、海报、支付营销推送
- 数据展示
- 整体闭环式服务

### 全家桶够劲，就它了

 - react  - github:https://github.com/facebook/react
 - react-router  - github:https://github.com/ReactTraining/react-router
 - redux    - github:https://github.com/reactjs/redux
 - react-router-redux   - github:https://github.com/reactjs/react-router-redux
 - redux-saga   - github:https://github.com/redux-saga/redux-saga
 - immutable  - github:https://github.com/facebook/immutable-js
 - reselect  - github:https://github.com/reactjs/reselect
 - antd   - github:https://github.com/ant-design/ant-design

## 学习教程
来源与台湾小贩的讲解[输入链接说明](https://github.com/bhnddowinf/vuejs2-learn)


## vue资源，堪称最完整的vue

[https://github.com/vuejs/awesome-vue](https://github.com/vuejs/awesome-vue)
